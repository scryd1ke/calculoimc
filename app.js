// Claudio Cabrera - Francisco Diaz
// Alejandro Adam - Allan Weisser

// INGRESAR PESO EN KILOS Y ALTURA EN CENTIMETROS

module.exports = function calculaIMC(peso,altura){

    //uso el metodo test para validar contra la expresion regular
    if (!/^([0-9])*$/.test(peso)){
        return("El valor ingresado de PESO no es un NUMERO");
    }

    else if (!/^([0-9])*$/.test(altura)){
        return("El valor ingresado de ALTURA no es un NUMERO");
    }

    else if(peso === ""){
      return "No ha ingresado el valor de su Peso, ingreselo por favor";
      }

    else if(altura === ""){
      return "No ha ingresado el valor de su Altura, ingreselo por favor";
      }

    else if (peso == 0) {
      return "Ha ingresado un Peso no valido, ingreselo nuevamente";
      }

    else if (altura == 0) {
      return "Ha ingresado una Altura no valida, ingreselo nuevamente";
      }

    else{
      altura = parseInt(altura) / 100;
      imc = (peso / (altura * altura));
      imc = imc.toFixed(2);
      return imc;

      if (imc < 18){
        return "Su IMC esta clasificado como BAJO PESO";
      }

      else if (imc >= 18 && imc < 25){
        return "Su IMC esta clasificado como NORMAL";
      }

      else if (imc >= 25 && imc < 27){
        return "Su IMC esta clasificado como SOBREPESO";
      }

      else if (imc >= 27 && imc < 30){
        return "Su IMC esta clasificado como OBESIDAD GRADO 1";
      }

      else if (imc >= 30 && imc < 40){
        return "Su IMC esta clasificado como OBESIDAD GRADO 2";
      }

      else {
        return "Su IMC esta clasificado como OBESIDAD GRADO 3";
      }

    }

}
