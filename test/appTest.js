// Claudio Cabrera - Francisco Diaz
// Alejandro Adam - Allan Weisser

// INGRESAR PESO EN KILOS Y ALTURA EN CENTIMETROS

var assert = require("chai").assert;
var calculaIMC = require("../app");

describe("PRUEBAS UNITARIAS PARA CALCULO DE IMC", function(){
  it("1 Prueba Calculo de IMC - Valores CORRECTOS", function(){
    assert.equal(calculaIMC(90,172),'30.42');
    
  });

  it("2 Prueba Calculo de IMC - NO ingreso un NUMERO como PESO", function(){
    assert.equal(calculaIMC("e",180),"El valor ingresado de PESO no es un NUMERO");
  });

  it("3 Prueba Calculo de IMC - NO ingreso un NUMERO como ALTURA", function(){
    assert.equal(calculaIMC(90,"*HOLA*"),"El valor ingresado de ALTURA no es un NUMERO");
  });

  it("4 Prueba Calculo de IMC - NO ingreso PESO", function(){
    assert.equal(calculaIMC("",180),"No ha ingresado el valor de su Peso, ingreselo por favor");
  });

  it("5 Prueba Calculo de IMC - NO ingreso ALTURA", function(){
    assert.equal(calculaIMC(80,""),"No ha ingresado el valor de su Altura, ingreselo por favor");
  });

  it("6 Prueba Calculo de IMC - Ingreso PESO = 0", function(){
    assert.equal(calculaIMC(0,180),"Ha ingresado un Peso no valido, ingreselo nuevamente");
  });

  it("7 Prueba Calculo de IMC - Ingreso ALTURA = 0", function(){
    assert.equal(calculaIMC(90,0),"Ha ingresado una Altura no valida, ingreselo nuevamente");
  });

  it("8 Prueba Calculo de IMC - BAJO PESO", function(){
    assert.equal(calculaIMC(50,185),(50/(1.85*1.85)).toFixed(2));
  });

  it("9 Prueba Calculo de IMC - NORMAL", function(){
    assert.equal(calculaIMC(70,175),(70/(1.75*1.75)).toFixed(2));
  });

  it("10 Prueba Calculo de IMC - SOBREPESO", function(){
    assert.equal(calculaIMC(75,170),(75/(1.7*1.7)).toFixed(2));
  });

  it("11 Prueba Calculo de IMC - OBESIDAD GRADO 1", function(){
    assert.equal(calculaIMC(80,170),(80/(1.7*1.7)).toFixed(2));
  });

  it("12 Prueba Calculo de IMC - OBESIDAD GRADO 2", function(){
    assert.equal(calculaIMC(95,165),(95/(1.65*1.65)).toFixed(2));
  });

  it("13 Prueba Calculo de IMC - OBESIDAD GRADO 3", function(){
    assert.equal(calculaIMC(130,165),(130/(1.65*1.65)).toFixed(2));
  });





});
